#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2013-2014, NewAE Technology Inc
# All rights reserved.
#
# Authors: Colin O'Flynn
#
# Find this and more at newae.com - this file is part of the chipwhisperer
# project, http://www.assembla.com/spaces/chipwhisperer
#
#    This file is part of chipwhisperer.
#
#    chipwhisperer is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    chipwhisperer is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with chipwhisperer.  If not, see <http://www.gnu.org/licenses/>.
#=================================================

import chipwhisperer as cw
from chipwhisperer.common.api.CWCoreAPI import CWCoreAPI  # Import the ChipWhisperer API
from chipwhisperer.common.scripts.base import UserScriptBase

import logging
logging.basicConfig(level=logging.ERROR)

import subprocess
import time
import argparse
import os
import glob
import numpy as np

import inspect

import datetime

def run(api, program):

    api.newProject()
    api.saveProject("./projects/glitch.cwp")

    print "Software Setup - 1. Connect to the ChipWhisperer device:"
    api.setParameter(['Generic Settings', 'Scope Module', 'ChipWhisperer/OpenADC'])
    api.setParameter(['Generic Settings', 'Target Module', 'Simple Serial'])
    api.setParameter(['Generic Settings', 'Trace Format', 'ChipWhisperer/Native'])
    api.setParameter(['Generic Settings', 'Auxiliary Module', 'None'])

    api.setParameter(['Generic Settings', 'Acquisition Settings', 'Number of Traces', 1])
    api.setParameter(['Generic Settings', 'Acquisition Settings', 'Number of Sets', 1])
    api.setParameter(['Generic Settings', 'Acquisition Settings', 'Key/Text Pattern', 'Basic'])

    api.setParameter(['Generic Settings', 'Basic', 'Key', 'Random'])
    api.setParameter(['Generic Settings', 'Basic', 'Plaintext', 'Random'])

    api.setParameter(['Simple Serial', 'Connection', 'NewAE USB (CWLite/CW1200)'])
    api.setParameter(['ChipWhisperer/OpenADC', 'Connection', 'NewAE USB (CWLite/CW1200)'])

    api.connect()

    api.setParameter(['OpenADC', 'Clock Setup', 'Freq Counter Src', 'CLKGEN Output'])
    api.setParameter(['OpenADC', 'Clock Setup', 'CLKGEN Settings', 'Desired Frequency', 7370000.0])
    api.setParameter(['OpenADC', 'Clock Setup', 'ADC Clock', 'Reset ADC DCM', None])
    api.setParameter(['OpenADC', 'Trigger Setup', 'Offset', 0])
    api.setParameter(['OpenADC', 'Trigger Setup', 'Total Samples', 800])
    api.setParameter(['Glitch Module', 'Clock Source', 'CLKGEN'])
    api.setParameter(['CW Extra Settings', 'Target HS IO-Out', 'Glitch Module'])

    api.setParameter(['CW Extra Settings', 'Target IOn Pins', 'Target IO1', 'Serial RXD'])
    api.setParameter(['CW Extra Settings', 'Target IOn Pins', 'Target IO2', 'Serial TXD'])
    api.setParameter(['Simple Serial', 'Load Key Command', u'k00000000000000000000000000000000\n'])
    api.setParameter(['Simple Serial', 'Go Command', u'p12341234123412341234123412341234\n'])
    api.setParameter(['Simple Serial', 'Output Format', u'r$RESPONSE$\n'])

    print "Flashing firmware"
    if program:
        subprocess.call("cd /export/home/bc254497/Travaux/openOCD/; ./config.sh", shell=True)

    api.setParameter(['Glitch Module', 'Glitch Offset (as % of period)', -20])
    api.setParameter(['Glitch Module', 'Glitch Width (as % of period)', 4])
    api.setParameter(['Glitch Module', 'Glitch Trigger', 'Ext Trigger:Single-Shot'])
    api.setParameter(['Glitch Module', 'Single-Shot Arm', 'After Scope Arm'])

    offsets = range(860, 1150)
    shots = range(10, 20)
    repeats = 1
    with open(timestamp+"/parameters.csv", "w") as param:
        param.write("offset,nb_shots\n")
        for offset in offsets:
            api.setParameter(['Glitch Module', 'Ext Trigger Offset', offset])
            for shot in shots:
                api.setParameter(['Glitch Module', 'Repeat', shot])
                print "Current parameters : {}/[{}-{}], {}/[{}-{}]".format(offset, min(offsets), max(offsets),
                                                                                                 shot, min(shots), max(shots))
                for repeat in range(repeats):
                    param.write(str(offset)+","+str(shot)+"\n")
                    print "Resetting target"
                    time.sleep(1)
                    api.setParameter(['CW Extra Settings', 'Target Power State', False]) # Turn off target
                    time.sleep(0.1)
                    api.setParameter(['CW Extra Settings', 'Target Power State', True])  # Turn on target
                    time.sleep(0.4)
                    api.captureM()

        api.saveProject("./projects/glitch.cwp")

    api.disconnect()

def remove_files():
    print "Removing useless files"
    files = glob.glob("./projects/glitch_data/traces/*_traces.npy")
    for f in files[1:]:
        os.remove(f)
    for f in glob.glob("./projects/glitch_data/traces/*_keylist.npy"):
        os.remove(f)
    for f in glob.glob("./projects/glitch_data/traces/*_knownkey.npy"):
        os.remove(f)
    for f in glob.glob("./projects/glitch_data/traces/*_textin.npy"):
        os.remove(f)
    for f in glob.glob("./projects/glitch_data/traces/*_settings.cwset"):
        os.remove(f)
    for f in glob.glob("./projects/glitch_data/traces/*.cfg"):
        os.remove(f)

def merge_files(timestamp):
    files = glob.glob("./projects/glitch_data/traces/*_textout.npy")
    files.sort(key=os.path.getmtime)
    array = np.ones((1, 16), dtype=np.uint8)
    for f in files:
        row = np.load(f)
        if np.shape(row) != (1, 16):
            print "Invalid array"
            row = np.ones((1, 16), dtype=np.uint8)
        array = np.vstack((array, row))
        os.remove(f)
    np.save(timestamp+"/glitch_results.npy", array[1:,:])

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Glitch performer')
    parser.add_argument("-p", "--program", action='store_true')
    args = parser.parse_args()
    api = CWCoreAPI()                                           # Instantiate the API

    time_now = datetime.datetime.now()
    timestamp = str(time_now.year)+"."+str(time_now.month)+"."+str(time_now.day)+"-"+str(time_now.hour)+"."+str(time_now.minute)+"."+str(time_now.second)
    os.mkdir(timestamp)

    run(api, args.program)
    merge_files(timestamp)
    remove_files()
