import matplotlib.pyplot as plt
import numpy as np
import os
import csv
import argparse

def analyse_glitch(directory, nb_dimensions, verbose):
    textout = np.load(os.path.join(directory, "glitch_results.npy"))
    parameters_file = open(os.path.join(directory, 'parameters.csv'), 'r')
    reader = csv.reader(parameters_file)
    parameters_names = reader.next()

    no_fault_parameters_x = []
    no_fault_parameters_y = []
    fault_parameters_x = []
    fault_parameters_y = []
    reset_parameters_x = []
    reset_parameters_y = []
    all_parameters_x = []
    all_parameters_y = []
    other_parameters_x = []
    other_parameters_y = []

    if nb_dimensions == 2:
        for parameters, text in zip(reader, textout):
            if verbose:
                print parameters, text
            try:
                if (np.unique(text) == [0]).all():
                    no_fault_parameters_x.append(parameters[0])
                    no_fault_parameters_y.append(parameters[1])
                elif (np.unique(text) == [255]).all():
                    fault_parameters_x.append(parameters[0])
                    fault_parameters_y.append(parameters[1])
                elif (np.unique(text) == [1]).all():
                    reset_parameters_x.append(parameters[0])
                    reset_parameters_y.append(parameters[1])
                else:
                    other_parameters_x.append(parameters[0])
                    other_parameters_y.append(parameters[1])
            except:
                pass
            all_parameters_x.append(parameters[0])
            all_parameters_y.append(parameters[1])
        all_parameters_x = np.unique([float(p) for p in all_parameters_x])
        all_parameters_y = np.unique([float(p) for p in all_parameters_y])
        plt.scatter(no_fault_parameters_x, no_fault_parameters_y, marker = '.', color='g', label = "No fault", alpha=0.2)
        plt.scatter(fault_parameters_x, fault_parameters_y, s=30, facecolors='none', edgecolors='r', label = "Fault")
        plt.scatter(reset_parameters_x, reset_parameters_y, marker = 'x', color='b', label = "Invalid")
        plt.scatter(other_parameters_x, other_parameters_y, marker = '_', color='k', label = "Other")
        
        plt.xlim((min(all_parameters_x)-1, max(all_parameters_x)+1))
        plt.xticks(np.hstack((all_parameters_x[::20],[max(all_parameters_x)])))
        plt.xlabel(parameters_names[0])
        
        plt.ylim((min(all_parameters_y)-1, max(all_parameters_y)+1))
        plt.yticks(all_parameters_y)
        plt.ylabel(parameters_names[1])

        plt.tight_layout()
        plt.legend(ncol=10, loc="upper center")
        plt.savefig(os.path.join(directory, "glitch_results.png"))
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Glitch analyzer')
    parser.add_argument("-d", "--directory", type=str)
    parser.add_argument("-n", "--nb_dimensions", type=int)
    parser.add_argument("-v", "--verbose", action='store_true')
    args = parser.parse_args()    
    analyse_glitch(args.directory, args.nb_dimensions, args.verbose)
